Pod::Spec.new do |s|

  # ―――----------------------------------――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.name         = "AuthnexUI"
  s.version      = "0.1.0"
  s.summary      = "Authnex UI SDK"
  s.description  = "Eliminate the hassle of forgotten passwords and the risk of stolen passwords with Authnex"
  s.documentation_url = 'https://bitbucket.org/aunex/authnexui/src/master/README.md'
  s.homepage     = "https://www.authnex.com"
  # s.license            = { :type => 'MIT', :file => 'LICENSE' }
  s.author             = { "Himal Madhushan" => "info@authnex.lk" }

  # ―――----------------------------------――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  s.platform     = :ios, "9.0"

  s.ios.vendored_frameworks = 'AuthnexUI.framework'

  s.source       = { :http => 'https://bitbucket.org/aunex/authnexui/raw/b0c85c5373d0c447ba77bdec306d29dd5c69700f/AuthnexUI.zip' }

  s.swift_version = '4.0'

  s.dependency 'UrbanAirship-iOS-SDK'
end