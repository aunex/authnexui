//
//  Utils.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    
    static let sharedInstance = Utils()
    
    private init() {}
}

extension Utils {
    func animateViweController(duration: CFTimeInterval, transitiontype: String, transitionsubtype: String, view: UIView) {
        let transition = CATransition()
        transition.duration = duration
        transition.type = CATransitionType(rawValue: transitiontype)
        transition.subtype = CATransitionSubtype(rawValue: transitionsubtype)
        view.window?.layer.add(transition, forKey: kCATransition)
    }
}

extension Utils {
    func animateView(duration: CFTimeInterval, transitiontype: String, transitionsubtype: String, view: UIView) {
        let transition = CATransition()
        transition.duration = duration
        transition.type = CATransitionType(rawValue: transitiontype)
        transition.subtype = CATransitionSubtype(rawValue: transitionsubtype)
        view.layer.add(transition, forKey: kCATransition)
    }
}
