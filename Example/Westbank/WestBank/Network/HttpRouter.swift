//
//  HttpRouter.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/28/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import Foundation
import Alamofire

enum HttpRouter: URLRequestConvertible {
    
    case userwestbankLogin(Login)
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .userwestbankLogin:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .userwestbankLogin:
            return HttpConstants.WESTBANK_LOGIN
        }
    }
    
    var baseUrl: String {
        switch self {
        case .userwestbankLogin:
            return HttpConstants.BASE_URL
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let url = URL(string: baseUrl+path)
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .userwestbankLogin(let login):
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: login.dictionary())
        }
    }
}
