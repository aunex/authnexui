//
//  Constants.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/28/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import Foundation

struct HttpConstants {
    
    static let BASE_URL = "http://aunexsimulator.com:15000/api"
    static let WESTBANK_LOGIN = "/AltLogin/loginservice"
}
