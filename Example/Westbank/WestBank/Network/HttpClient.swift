//
//  HttpClient.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/28/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import Foundation
import Alamofire

class HttpClient {
    
    static let sharedInstance = HttpClient()
    
    private init() { }
}

extension HttpClient {
    func loginWestBank(login: Login, completion: @escaping(Bool, String) ->()) {
        
        let urlrequest = HttpRouter.userwestbankLogin(login).urlRequest
        
        Alamofire.request(urlrequest!).responseJSON { (response: DataResponse<Any>) in
            switch response.result {
            case .success(let data):
                completion(true, "\(data)")
                return
            case .failure(let err):
                completion(false, err.localizedDescription)
            }
        }
    }
}
