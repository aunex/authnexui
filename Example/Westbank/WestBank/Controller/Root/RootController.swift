//
//  ViewController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/23/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

import AuthnexUI

class RootController: SharedController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigateTo()
    }
}

extension RootController {
    fileprivate func navigateTo() {
        
        var navcontorller: UIViewController!
        
        if (AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
            navcontorller = BankHomeController()
        } else {
            navcontorller = BankLoginController()
        }
        DispatchQueue.main.async {
            Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.fade.rawValue, transitionsubtype: CATransitionSubtype.fromTop.rawValue, view: self.view)
            self.present(navcontorller, animated: false, completion: nil)
        }
    }
}

