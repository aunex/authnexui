//
//  BanktoAuthnexAuthController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit
import AuthnexUI

class BanktoAuthnexAuthController: AuthnexAuthController {
    
    private let deviceHeight: CGFloat = UIScreen.main.bounds.height
    private let deviceWidth: CGFloat = UIScreen.main.bounds.width
    private var backButton: UIButton!
    
    //MARK: Mandatory for Authnex implementation
    var authverification: Bool = false
    var authverificationErrmessage: String?
    var authatm: Bool = false
    var authnexerrcode: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupMandatory()
        setbackButton()
    }
}

extension BanktoAuthnexAuthController {
    fileprivate func setupMandatory() {
        authnexErrorCode = authnexerrcode
        authnexVerification = authverification
        authnexVerificarionErr = authverificationErrmessage
    }
}

extension BanktoAuthnexAuthController {
    fileprivate func setbackButton() {
        backButton = UIButton()
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.backgroundColor = .clear
        backButton.setImage(UIImage(named: "backbutton"), for: .normal)
        backButton.imageView?.contentMode = .scaleAspectFit
        view.addSubview(backButton)
        
        let menubuttonTop = deviceHeight/33.35
        let menubuttonLeft = deviceWidth/37.5
        let menubuttonWidthHeight = deviceWidth/9.375
        let backbuttonConstraints = [backButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: menubuttonLeft),
                                     backButton.topAnchor.constraint(equalTo: view.topAnchor, constant: menubuttonTop),
                                     backButton.widthAnchor.constraint(equalToConstant: menubuttonWidthHeight),
                                     backButton.heightAnchor.constraint(equalToConstant: menubuttonWidthHeight)]
        NSLayoutConstraint.activate(backbuttonConstraints)
        backButton.addTarget(self, action: #selector(backActoin(_:)), for: .touchUpInside)
    }
}

extension BanktoAuthnexAuthController {
    @objc fileprivate func backActoin(_ sender: UIButton) {
        let rootController = RootController()
        Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromLeft.rawValue, view: self.view)
        self.present(rootController, animated: false, completion: nil)
    }
}
