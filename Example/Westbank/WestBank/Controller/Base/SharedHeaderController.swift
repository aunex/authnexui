//
//  SharedHeaderController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class SharedHeaderController: BaseController {

    public var headerView: UIView!
    public var headerviewTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderUI()
    }
}

extension SharedHeaderController {
    fileprivate func setHeaderUI() {
        headerView = UIView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = UIColor.init(red: 68/255, green: 8/255, blue: 99/255, alpha: 1)
        view.addSubview(headerView)
        
        let headerviewHeight = deviceHeight/10.42
        let headerviewConstraints = [headerView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                     headerView.topAnchor.constraint(equalTo: view.topAnchor),
                                     headerView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                     headerView.heightAnchor.constraint(equalToConstant: headerviewHeight)]
        NSLayoutConstraint.activate(headerviewConstraints)
        
        headerviewTitle = UILabel()
        headerviewTitle.translatesAutoresizingMaskIntoConstraints = false
        headerviewTitle.text = "GETTING STARTED"
        headerviewTitle.textColor = .white
        headerviewTitle.font = UIFont(name: "Verdana", size: 17 * fontScale)
        headerviewTitle.textAlignment = .center
        headerView.addSubview(headerviewTitle)
        
        let headerviewtitleConstraints = [headerviewTitle.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),
                                          headerviewTitle.centerYAnchor.constraint(equalTo: headerView.centerYAnchor)]
        NSLayoutConstraint.activate(headerviewtitleConstraints)
    }
}
