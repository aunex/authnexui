//
//  BaseController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class BaseController: UIViewController {
    
    let deviceHeight = UIScreen.main.bounds.height
    let deviceWidth = UIScreen.main.bounds.width
    let fontScale = UIScreen.main.bounds.width/375
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
