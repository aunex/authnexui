//
//  SharedController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class SharedController: UIViewController {
    
    let deviceHeight = UIScreen.main.bounds.height
    let deviceWidth = UIScreen.main.bounds.width
    let fontScale = UIScreen.main.bounds.width/375
    
    private var backgroundimgeView: UIImageView!
    private var banklogoImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setsharedUI()
    }
}

extension SharedController {
    fileprivate func setsharedUI() {
        backgroundimgeView = UIImageView()
        backgroundimgeView.translatesAutoresizingMaskIntoConstraints = false
        backgroundimgeView.contentMode = .scaleAspectFit
        backgroundimgeView.backgroundColor = UIColor.init(red: 60/255, green: 26/255, blue: 92/255, alpha: 1)
        backgroundimgeView.image = UIImage(named: "startupbg")
        backgroundimgeView.isOpaque = true
        backgroundimgeView.clearsContextBeforeDrawing = true
        backgroundimgeView.autoresizesSubviews = true
        view.addSubview(backgroundimgeView)
        let backgroundimageviewConstraints = [backgroundimgeView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                              backgroundimgeView.topAnchor.constraint(equalTo: view.topAnchor),
                                              backgroundimgeView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                              backgroundimgeView.bottomAnchor.constraint(equalTo: view.bottomAnchor)]
        NSLayoutConstraint.activate(backgroundimageviewConstraints)
        
        
        banklogoImageView = UIImageView()
        banklogoImageView.translatesAutoresizingMaskIntoConstraints = false
        banklogoImageView.contentMode = .scaleAspectFit
        banklogoImageView.backgroundColor = .clear
        banklogoImageView.image = UIImage(named: "logo")
        view.addSubview(banklogoImageView)
        
        let banklogoTop = deviceHeight/8.33
        let banklogoWidth = deviceWidth/1.70
        let banklogoHeight = deviceHeight/3.33
        let banklogoimageviewConstraints = [banklogoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                            banklogoImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: banklogoTop),
                                            banklogoImageView.widthAnchor.constraint(equalToConstant: banklogoWidth),
                                            banklogoImageView.heightAnchor.constraint(equalToConstant: banklogoHeight)]
        NSLayoutConstraint.activate(banklogoimageviewConstraints)
        
    }
}
