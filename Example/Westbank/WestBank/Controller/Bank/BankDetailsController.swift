//
//  BankSettingsController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit
import AuthnexUI

class BankDetailsController: SharedHeaderController {
    
    private var footerView: UIView!
    private var footerViewImage: UIImageView!
    private var cardlessTitle: UILabel!
    private var backButton: UIButton!
    private var detailtableView: UITableView!
    private let detailcellidentifierOne = "detailcellidentifierone"
    private let detailcellidentifierTwo = "detailcellidentifiertwo"
    private let detailcellidentifierThree = "detailcellidentifierthree"

    override func viewDidLoad() {
        super.viewDidLoad()
        setviewUI()
        setDetailsUI()
    }
}

extension BankDetailsController {
    fileprivate func setviewUI() {
        view.backgroundColor = .white
        headerviewTitle.text = "WEST BANK"
        
        backButton = UIButton()
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.setImage(UIImage(named: "backbutton"), for: .normal)
        view.addSubview(backButton)
        
        let menubuttonTop = deviceHeight/33.35
        let menubuttonLeft = deviceWidth/37.5
        let menubuttonWidthHeight = deviceWidth/9.375
        let backbuttonConstraints = [backButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: menubuttonLeft),
                                     backButton.topAnchor.constraint(equalTo: view.topAnchor, constant: menubuttonTop),
                                     backButton.widthAnchor.constraint(equalToConstant: menubuttonWidthHeight),
                                     backButton.heightAnchor.constraint(equalToConstant: menubuttonWidthHeight)]
        NSLayoutConstraint.activate(backbuttonConstraints)
        backButton.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)
        
        footerView = UIView()
        footerView.translatesAutoresizingMaskIntoConstraints = false
        footerView.backgroundColor = UIColor.init(red: 68/255, green: 8/255, blue: 99/255, alpha: 1)
        view.addSubview(footerView)
        
        let footerviewHeight = deviceHeight/9.52
        
        let footerviewConstraints = [footerView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                     footerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                                     footerView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                     footerView.heightAnchor.constraint(equalToConstant: footerviewHeight)]
        NSLayoutConstraint.activate(footerviewConstraints)
        
        footerViewImage = UIImageView()
        footerViewImage.translatesAutoresizingMaskIntoConstraints = false
        footerViewImage.backgroundColor = .clear
        footerViewImage.contentMode = .scaleAspectFit
        footerViewImage.image = UIImage(named: "qr")
        footerView.addSubview(footerViewImage)
        
        let footerviewimageConstraints = [footerViewImage.topAnchor.constraint(equalTo: footerView.topAnchor),
                                          footerViewImage.rightAnchor.constraint(equalTo: footerView.rightAnchor),
                                          footerViewImage.bottomAnchor.constraint(equalTo: footerView.bottomAnchor),
                                          footerViewImage.widthAnchor.constraint(equalToConstant: footerviewHeight)]
        NSLayoutConstraint.activate(footerviewimageConstraints)
        
        cardlessTitle = UILabel()
        cardlessTitle.translatesAutoresizingMaskIntoConstraints = false
        cardlessTitle.textAlignment = .right
        cardlessTitle.text = "Card-less ATM"
        cardlessTitle.textColor = .white
        cardlessTitle.font = UIFont(name: "Verdana-Bold", size: 20 * fontScale)
        footerView.addSubview(cardlessTitle)
        
        let cardlessatmright = deviceWidth/37.5
        let cardlesstitleConstraints = [cardlessTitle.centerYAnchor.constraint(equalTo: footerView.centerYAnchor),
                                        cardlessTitle.rightAnchor.constraint(equalTo: footerViewImage.leftAnchor, constant: -cardlessatmright)]
        NSLayoutConstraint.activate(cardlesstitleConstraints)
    }
}

extension BankDetailsController {
    @objc fileprivate func backAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromLeft.rawValue, view: self.view)
            self.dismiss(animated: false, completion: nil)
        }
    }
}

extension BankDetailsController {
    fileprivate func setDetailsUI() {
        detailtableView = UITableView(frame: .zero, style: .plain)
        detailtableView.translatesAutoresizingMaskIntoConstraints = false
        detailtableView.register(DetailNameCell.self, forCellReuseIdentifier: detailcellidentifierOne)
        detailtableView.register(DetailAccountCell.self, forCellReuseIdentifier: detailcellidentifierTwo)
        detailtableView.register(UITableViewCell.self, forCellReuseIdentifier: detailcellidentifierThree)
        detailtableView.dataSource = self
        detailtableView.delegate = self
        detailtableView.tableFooterView = UIView()
        detailtableView.separatorStyle = .none
        view.addSubview(detailtableView)
        
        let detailtableviewConstraints = [detailtableView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                          detailtableView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
                                          detailtableView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                          detailtableView.bottomAnchor.constraint(equalTo: footerView.topAnchor)]
        NSLayoutConstraint.activate(detailtableviewConstraints)
    }
}

extension BankDetailsController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let detailcellOne = tableView.dequeueReusableCell(withIdentifier: detailcellidentifierOne) as! DetailNameCell
            return detailcellOne
        case 1:
            let detailcellTwo = tableView.dequeueReusableCell(withIdentifier: detailcellidentifierTwo) as! DetailAccountCell
            return detailcellTwo
        default:
            let detailcellThree = tableView.dequeueReusableCell(withIdentifier: detailcellidentifierThree)
            detailcellThree?.accessoryType = .disclosureIndicator
            detailcellThree?.textLabel?.textColor = UIColor.init(red: 0/255, green: 182/255, blue: 190/255, alpha: 1)
            detailcellThree?.tintColor = UIColor.init(red: 0/255, green: 182/255, blue: 190/255, alpha: 1)
            detailcellThree?.textLabel?.font = UIFont(name: "Verdana", size: 17 * fontScale)
            if indexPath.row == 2 {
                detailcellThree?.textLabel?.text = "Logout"
            }
            if indexPath.row == 3 {
                detailcellThree?.textLabel?.text = "Fund Transfer Management"
            }
            if indexPath.row == 4 {
                detailcellThree?.textLabel?.text = "Investment Management"
            }
            if indexPath.row == 5 {
                detailcellThree?.textLabel?.text = "Settings"
            }
            
            return detailcellThree!
        }
    }
}

extension BankDetailsController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let header = headerView.frame.height
        let footer = footerView.frame.height
        let restHeight = deviceHeight - (header + footer)
        let rowHeight = restHeight/10
        switch indexPath.row {
        case 0:
            return rowHeight * 2
        case 1:
            return rowHeight * 4
        default:
            return rowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            Logout()
        }
    }
}

extension BankDetailsController {
    fileprivate func Logout() {
        AuthnexCore.sharedInctance.clearAll()
        let rootController = RootController()
        Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromLeft.rawValue, view: self.view)
        self.present(rootController, animated: false, completion: nil)
    }
}

