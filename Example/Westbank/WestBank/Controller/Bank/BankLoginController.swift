//
//  BankLoginController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class BankLoginController: SharedController {

    private var loginButton: UIButton!
    private var passwordText: UITextField!
    private var usernameText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setloginUI()
    }
}

extension BankLoginController {
    fileprivate func setloginUI() {
        loginButton = UIButton()
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.backgroundColor = UIColor.init(red: 0/255, green: 182/255, blue: 190/255, alpha: 1)
        loginButton.setTitle("LOGIN", for: .normal)
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.titleLabel?.font = UIFont(name: "Helvetica", size: 30 * fontScale)
        view.addSubview(loginButton)
        
        let loginbuttonBottom = -deviceHeight/13.34
        let loginbuttonWidth = deviceWidth - 2*deviceWidth/17.04
        let loginbuttonHeight = deviceHeight/13.34
        let loginbuttonConstraints = [loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                      loginButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: loginbuttonBottom),
                                      loginButton.widthAnchor.constraint(equalToConstant: loginbuttonWidth),
                                      loginButton.heightAnchor.constraint(equalToConstant: loginbuttonHeight)]
        NSLayoutConstraint.activate(loginbuttonConstraints)
        loginButton.addTarget(self, action: #selector(LoginAction(_:)), for: .touchUpInside)
        
        let textfieldBottom = -deviceHeight/33.35
        
        passwordText = UITextField()
        passwordText.translatesAutoresizingMaskIntoConstraints = false
        passwordText.borderStyle = .none
        passwordText.isSecureTextEntry = true
        passwordText.textAlignment = .center
        passwordText.font = UIFont(name: "Helvetica", size: 17 * fontScale)
        passwordText.placeholder = "✽✽✽✽✽✽✽✽✽"
        passwordText.textColor = .white
        passwordText.returnKeyType = .done
        view.addSubview(passwordText)
        passwordText.delegate = self
        
        let passwordtextConstraints = [passwordText.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                       passwordText.bottomAnchor.constraint(equalTo: loginButton.topAnchor, constant: textfieldBottom),
                                       passwordText.widthAnchor.constraint(equalToConstant: loginbuttonWidth),
                                       passwordText.heightAnchor.constraint(equalToConstant: loginbuttonHeight)]
        NSLayoutConstraint.activate(passwordtextConstraints)
        
        let bottomLine1 = CALayer()
        bottomLine1.frame = CGRect(x: 0.0, y: loginbuttonHeight - deviceWidth/375, width: loginbuttonWidth, height: deviceWidth/375)
        bottomLine1.backgroundColor = UIColor.white.cgColor
        passwordText.layer.addSublayer(bottomLine1)
        
        usernameText = UITextField()
        usernameText.translatesAutoresizingMaskIntoConstraints = false
        usernameText.borderStyle = .none
        usernameText.isSecureTextEntry = false
        usernameText.autocorrectionType = .no
        usernameText.textAlignment = .center
        usernameText.font = UIFont(name: "Helvetica", size: 17 * fontScale)
        usernameText.placeholder = "USERNAME"
        usernameText.keyboardType = .emailAddress
        usernameText.autocapitalizationType = .none
        usernameText.textColor = .white
        usernameText.returnKeyType = .done
        view.addSubview(usernameText)
        usernameText.delegate = self
        
        let usernameTextConstraints = [usernameText.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                       usernameText.bottomAnchor.constraint(equalTo: passwordText.topAnchor, constant: textfieldBottom),
                                       usernameText.widthAnchor.constraint(equalToConstant: loginbuttonWidth),
                                       usernameText.heightAnchor.constraint(equalToConstant: loginbuttonHeight)]
        NSLayoutConstraint.activate(usernameTextConstraints)
        
        let bottomLine2 = CALayer()
        bottomLine2.frame = CGRect(x: 0.0, y: loginbuttonHeight - deviceWidth/375, width: loginbuttonWidth, height: deviceWidth/375)
        bottomLine2.backgroundColor = UIColor.white.cgColor
        usernameText.layer.addSublayer(bottomLine2)
        
        usernameText.text = "shivitest@gmail.com"
        passwordText.text = "Anu@123e"
        
        usernameText.text = "testa6@t.com"
        passwordText.text = "Password@1"
    }
}

extension BankLoginController {
    @objc fileprivate func LoginAction(_ sender: UIButton) {
        
        let login = Login()
        login.UserName = usernameText.text
        login.Passsword = passwordText.text
        
        login.logintoWestBank(login: login) { (success, err) in
            if success {
                let bankintroviewcontroller = BankIntroController()
                DispatchQueue.main.async {
                    Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromRight.rawValue, view: self.view)
                    self.present(bankintroviewcontroller, animated: false, completion: nil)
                }
            } else {
                print("Error Login: ", err)
            }
        }
    }
}

extension BankLoginController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
