//
//  BankHomeController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit
import AuthnexUI

class BankHomeController: SharedController {
    
    private var menucollectionView: UICollectionView!
    private let menucellIdentifier = "menucellIdentifier"
    private var btnLogout: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setbankhomeUI()
    }
}

extension BankHomeController {
    fileprivate func setbankhomeUI() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        menucollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        menucollectionView.translatesAutoresizingMaskIntoConstraints = false
        menucollectionView.register(MenuCell.self, forCellWithReuseIdentifier: menucellIdentifier)
        menucollectionView.dataSource = self
        menucollectionView.delegate = self
        view.addSubview(menucollectionView)
        
        btnLogout = UIButton(frame: .zero)
        btnLogout.translatesAutoresizingMaskIntoConstraints = false
        btnLogout.setTitle("Logout", for: .normal)
        btnLogout.setTitleColor(.white, for: .normal)
        btnLogout.addTarget(self, action: #selector(logout), for: .touchUpInside)
        view.addSubview(btnLogout)
        
        let buttonConstraints = [btnLogout.topAnchor.constraint(equalTo: view.topAnchor, constant: 30),
                                 btnLogout.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8),
                                 btnLogout.heightAnchor.constraint(equalToConstant: 40)]
        NSLayoutConstraint.activate(buttonConstraints)
        
        let menucollectionviewHeight = 3*deviceHeight/11.11
        let menucollectionviewConstraints = [menucollectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                             menucollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                                             menucollectionView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                             menucollectionView.heightAnchor.constraint(equalToConstant: menucollectionviewHeight)]
        NSLayoutConstraint.activate(menucollectionviewConstraints)
    }
    
    @objc func logout() {
//        dismiss(animated: true)
        let login = BankLoginController()
        DispatchQueue.main.async {
            AuthnexCore.sharedInctance.clearAll()
            Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromRight.rawValue, view: self.view)
            self.present(login, animated: false, completion: nil)
        }
    }
}

extension BankHomeController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let menucell = collectionView.dequeueReusableCell(withReuseIdentifier: menucellIdentifier, for: indexPath) as! MenuCell
        menucell.rowindex = indexPath.row
        menucell.backgroundColor = .white
        return menucell
    }
}

extension BankHomeController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigation(index: indexPath.row)
    }
}

extension BankHomeController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = deviceWidth
        let cellHeight = deviceHeight/11.11
        return CGSize(width: cellWidth, height: cellHeight)
    }
}

extension BankHomeController {
    fileprivate func navigation(index: Int) {
        var navcontroller: UIViewController!
        AuthnexCore.sharedInctance.localAuthentication { (authenticate, err, errcode) in
            if authenticate {
                switch index {
                case 0:
                    navcontroller = BanktoAuthnexATMController()
                case 1:
                    navcontroller = BankDetailsController()
                case 2:
                    navcontroller = BanktoAuthnexAuthController()
                default:
                    return
                }
                DispatchQueue.main.async {
                    Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromRight.rawValue, view: self.view)
                    self.present(navcontroller, animated: false, completion: nil)
                }
            }
        }
    }
}
