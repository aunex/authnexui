//
//  BankIntroController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class BankIntroController: SharedController {

    private var footerView: UIView!
    private var pageController: UIPageControl!
    private var skipButton: UIButton!
    private var nextButton: UIButton!
    private var headerView: UIView!
    private var headerviewTitle: UILabel!
    private var slidercollectionView: UICollectionView!
    private let slidercellIdentifier = "slidercellIdentifier"
    private var currentIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setHeaderFooterUI()
        setsliderUI()
        
    }
}

extension BankIntroController {
    fileprivate func setHeaderFooterUI() {
        footerView = UIView()
        footerView.translatesAutoresizingMaskIntoConstraints = false
        footerView.backgroundColor = .white
        view.addSubview(footerView)
        
        let footerviewHeight = deviceHeight/13.34
        let footerviewConstraints = [footerView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                     footerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                                     footerView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                     footerView.heightAnchor.constraint(equalToConstant: footerviewHeight)]
        NSLayoutConstraint.activate(footerviewConstraints)
        
        pageController = UIPageControl()
        pageController.translatesAutoresizingMaskIntoConstraints = false
        pageController.numberOfPages = 3
        pageController.currentPageIndicatorTintColor = UIColor.init(red: 68/255, green: 8/255, blue: 99/255, alpha: 1)
        pageController.pageIndicatorTintColor = .lightGray
        pageController.transform = CGAffineTransform(scaleX: fontScale, y: fontScale)
        footerView.addSubview(pageController)
        
        let pagecontrollerConstraints = [pageController.centerXAnchor.constraint(equalTo: footerView.centerXAnchor),
                                         pageController.centerYAnchor.constraint(equalTo: footerView.centerYAnchor)]
        NSLayoutConstraint.activate(pagecontrollerConstraints)
        
        skipButton = UIButton()
        skipButton.translatesAutoresizingMaskIntoConstraints = false
        skipButton.setTitle("SKIP", for: .normal)
        skipButton.titleLabel?.font = UIFont(name: "Verdana", size: 14 * fontScale)
        skipButton.setTitleColor(.lightGray, for: .normal)
        footerView.addSubview(skipButton)
        
        let buttonleftright = deviceWidth/18.57
        let buttonwidthheight = deviceHeight/13.34
        let skipbuttonConstraints = [skipButton.centerYAnchor.constraint(equalTo: footerView.centerYAnchor),
                                     skipButton.leftAnchor.constraint(equalTo: footerView.leftAnchor, constant: buttonleftright),
                                     skipButton.widthAnchor.constraint(equalToConstant: buttonwidthheight),
                                     skipButton.heightAnchor.constraint(equalToConstant: buttonwidthheight)]
        NSLayoutConstraint.activate(skipbuttonConstraints)
        skipButton.addTarget(self, action: #selector(skipAction(_:)), for: .touchUpInside)
        
        nextButton = UIButton()
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        nextButton.setTitle("NEXT", for: .normal)
        nextButton.titleLabel?.font = UIFont(name: "Verdana", size: 14 * fontScale)
        nextButton.setTitleColor(UIColor.init(red: 68/255, green: 8/255, blue: 99/255, alpha: 1), for: .normal)
        footerView.addSubview(nextButton)
        
        let nextButtonConstraints = [nextButton.centerYAnchor.constraint(equalTo: footerView.centerYAnchor),
                                     nextButton.rightAnchor.constraint(equalTo: footerView.rightAnchor, constant: -buttonleftright),
                                     nextButton.widthAnchor.constraint(equalToConstant: buttonwidthheight),
                                     nextButton.heightAnchor.constraint(equalToConstant: buttonwidthheight)]
        NSLayoutConstraint.activate(nextButtonConstraints)
        nextButton.addTarget(self, action: #selector(nextAction(_:)), for: .touchUpInside)
        
        headerView = UIView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = UIColor.init(red: 68/255, green: 8/255, blue: 99/255, alpha: 1)
        view.addSubview(headerView)
        
        let headerviewHeight = deviceHeight/10.42
        let headerviewConstraints = [headerView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                     headerView.topAnchor.constraint(equalTo: view.topAnchor),
                                     headerView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                     headerView.heightAnchor.constraint(equalToConstant: headerviewHeight)]
        NSLayoutConstraint.activate(headerviewConstraints)
        
        headerviewTitle = UILabel()
        headerviewTitle.translatesAutoresizingMaskIntoConstraints = false
        headerviewTitle.text = "GETTING STARTED"
        headerviewTitle.textColor = .white
        headerviewTitle.font = UIFont(name: "Verdana", size: 17 * fontScale)
        headerviewTitle.textAlignment = .center
        headerView.addSubview(headerviewTitle)
        
        let headerviewtitleConstraints = [headerviewTitle.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),
                                          headerviewTitle.centerYAnchor.constraint(equalTo: headerView.centerYAnchor)]
        NSLayoutConstraint.activate(headerviewtitleConstraints)
    }
}

extension BankIntroController {
    fileprivate func setsliderUI() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        slidercollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        slidercollectionView.translatesAutoresizingMaskIntoConstraints = false
        slidercollectionView.register(SliderCell.self, forCellWithReuseIdentifier: slidercellIdentifier)
        slidercollectionView.dataSource = self
        slidercollectionView.delegate = self
        slidercollectionView.showsHorizontalScrollIndicator = false
        slidercollectionView.isPagingEnabled = true
        slidercollectionView.backgroundColor = .white
        view.addSubview(slidercollectionView)
        
        let slidercollectionviewConstraints = [slidercollectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                               slidercollectionView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
                                               slidercollectionView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                               slidercollectionView.bottomAnchor.constraint(equalTo: footerView.topAnchor)]
        NSLayoutConstraint.activate(slidercollectionviewConstraints)
    }
}


extension BankIntroController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let slidercell = collectionView.dequeueReusableCell(withReuseIdentifier: slidercellIdentifier, for: indexPath) as! SliderCell
        slidercell.rowIndex = indexPath.row
        return slidercell
    }
}

extension BankIntroController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = deviceWidth
        let cellHeight = deviceHeight - (headerView.frame.height + footerView.frame.height)
        return CGSize(width: cellWidth, height: cellHeight)
    }
}

extension BankIntroController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        currentIndex = Int(scrollView.contentOffset.x/(deviceWidth))
        pageController.currentPage = currentIndex
    }
}

extension BankIntroController {
    @objc fileprivate func skipAction(_ sender: UIButton) {
        let bankagreementviewController = BankAgreementController()
        DispatchQueue.main.async {
            Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromRight.rawValue, view: self.view)
            self.present(bankagreementviewController, animated: false, completion: nil)
        }
    }
    
    @objc fileprivate func nextAction(_ sender: UIButton) {
        currentIndex = currentIndex + 1
        if currentIndex < 3 {
            slidercollectionView.scrollToItem(at: IndexPath(row: currentIndex, section: 0), at: .right, animated: true)
        } else {
            if currentIndex == 3 {
                let bankagreementController = BankAgreementController()
                DispatchQueue.main.async {
                    Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromRight.rawValue, view: self.view)
                    self.present(bankagreementController, animated: false, completion: nil)
                }
            }
        }
    }
}
