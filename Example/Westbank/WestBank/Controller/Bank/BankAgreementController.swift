//
//  BankAgreementController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class BankAgreementController: SharedHeaderController {

    private var activelaterButton: UIButton!
    private var agreeButton: UIButton!
    private var webView: UIWebView!
    private var activitiindicatorContainer: UIView!
    private var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setviewUI()
        setHtml()
    }
}

extension BankAgreementController {
    fileprivate func setviewUI() {
        view.backgroundColor = .white
        headerviewTitle.text = "TERMS & CONDITIONS"
        
        activelaterButton = UIButton()
        activelaterButton.translatesAutoresizingMaskIntoConstraints = false
        activelaterButton.setTitle("Active Later", for: .normal)
        activelaterButton.setTitleColor(.black, for: .normal)
        activelaterButton.titleLabel?.font = UIFont(name: "Helvetica", size: 20 * fontScale)
        view.addSubview(activelaterButton)
        
        let activelaterBottom = -deviceHeight/66.67
        let activelaterHeight = deviceHeight/16.675
        let activelaterbuttonConstraints = [activelaterButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                            activelaterButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: activelaterBottom),
                                            activelaterButton.widthAnchor.constraint(equalToConstant: deviceWidth),
                                            activelaterButton.heightAnchor.constraint(equalToConstant: activelaterHeight)]
        NSLayoutConstraint.activate(activelaterbuttonConstraints)
        activelaterButton.addTarget(self, action: #selector(activeLater(_:)), for: .touchUpInside)
        
        agreeButton = UIButton()
        agreeButton.translatesAutoresizingMaskIntoConstraints = false
        agreeButton.setTitle("I Agree And Proceed", for: .normal)
        agreeButton.setTitleColor(.white, for: .normal)
        agreeButton.backgroundColor = UIColor.init(red: 0/255, green: 182/255, blue: 190/255, alpha: 1)
        agreeButton.titleLabel?.font = UIFont(name: "Helvetica-Bold", size: 20 * fontScale)
        view.addSubview(agreeButton)
        
        let agreeButtonBottom = -deviceHeight/66.67
        let agreeButtonHeight = deviceHeight/13.34
        let agreeButtonWidth = deviceWidth - 2*deviceWidth/17.04
        let agreeButtonConstraints = [agreeButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                            agreeButton.bottomAnchor.constraint(equalTo: activelaterButton.topAnchor, constant: agreeButtonBottom),
                                            agreeButton.widthAnchor.constraint(equalToConstant: agreeButtonWidth),
                                            agreeButton.heightAnchor.constraint(equalToConstant: agreeButtonHeight)]
        NSLayoutConstraint.activate(agreeButtonConstraints)
        agreeButton.addTarget(self, action: #selector(agreenProceed(_:)), for: .touchUpInside)
        
        webView = UIWebView()
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.backgroundColor = .clear
        view.addSubview(webView)
        let webviweConstraints = [webView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                  webView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
                                  webView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                  webView.bottomAnchor.constraint(equalTo: agreeButton.topAnchor)]
        NSLayoutConstraint.activate(webviweConstraints)
        webView.delegate = self
        
        activitiindicatorContainer = UIView()
        activitiindicatorContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activitiindicatorContainer)
        
        let activityindicatorcontainerConstraints = [activitiindicatorContainer.leftAnchor.constraint(equalTo: view.leftAnchor),
                                                     activitiindicatorContainer.topAnchor.constraint(equalTo: view.topAnchor),
                                                     activitiindicatorContainer.rightAnchor.constraint(equalTo: view.rightAnchor),
                                                     activitiindicatorContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)]
        NSLayoutConstraint.activate(activityindicatorcontainerConstraints)
        
        activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.color = UIColor.init(red: 68/255, green: 8/255, blue: 99/255, alpha: 1)
        activitiindicatorContainer.addSubview(activityIndicator)
        
        let activityindicatorConstraints = [activityIndicator.centerXAnchor.constraint(equalTo: activitiindicatorContainer.centerXAnchor),
                                            activityIndicator.centerYAnchor.constraint(equalTo: activitiindicatorContainer.centerYAnchor)]
        NSLayoutConstraint.activate(activityindicatorConstraints)
        activityIndicator.startAnimating()
        
    }
}

extension BankAgreementController {
    fileprivate func setHtml() {
        let url = Bundle.main.url(forResource: "privacy", withExtension: "html")
        let urlRequest = URLRequest(url: url!)
        webView.loadRequest(urlRequest)
    }
}

extension BankAgreementController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activitiindicatorContainer.isHidden = true
    }
}

extension BankAgreementController {
    @objc fileprivate func activeLater(_ sender: UIButton) {
        let bankhomeController = BankHomeController()
        DispatchQueue.main.async {
            Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromRight.rawValue, view: self.view)
            self.present(bankhomeController, animated: false, completion: nil)
        }
    }
    
    @objc fileprivate func agreenProceed(_ sender: UIButton) {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        let banktoauthnexregController = BanktoAuthnexRegistrationController(collectionViewLayout: layout)
        DispatchQueue.main.async {
            Utils.sharedInstance.animateViweController(duration: 0.4, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromRight.rawValue, view: self.view)
            self.present(banktoauthnexregController, animated: false, completion: nil)
        }
    }
}
