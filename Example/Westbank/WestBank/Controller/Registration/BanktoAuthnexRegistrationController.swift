//
//  BanktoAuthnexRegistrationController.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit
import AuthnexUI

class BanktoAuthnexRegistrationController: AuthnexRegistrationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setauthnexMandatory()
        authnexregistrationdelegate = self
    }
}

extension BanktoAuthnexRegistrationController {
    fileprivate func setauthnexMandatory() {
        iscoperate = true
        registrationtype = .qr
        reg_authnexUserName = "WestBank Username" // any user name
        reg_authnexFirstName = "WestBank" // any first name
        reg_authnexLastName = "Platform" // any last name
        reg_authnexCompanyName = "WestBank" // any company name
        reg_authnexDivision = "Security" // any division
        reg_authnexuserEmail = "admin@westbank.com" // any email
        reg_authnexMobileNumber = "0761111111" //any ten digits number
    }
}

extension BanktoAuthnexRegistrationController: AuthnexRegistrationDelegate {
    func authnexregistrationSuccess(registrationSuccess: Bool, LaunchQR: Bool, errormessage: String?) {
        if registrationSuccess {
            if LaunchQR {
                let authController = BanktoAuthnexAuthController()
                DispatchQueue.main.async {
                    Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromRight.rawValue, view: self.view)
                    self.present(authController, animated: false, completion: nil)
                }
            } else {
                let bankhomeController = BankHomeController()
                DispatchQueue.main.async {
                    Utils.sharedInstance.animateViweController(duration: 0.3, transitiontype: CATransitionType.push.rawValue, transitionsubtype: CATransitionSubtype.fromRight.rawValue, view: self.view)
                    self.present(bankhomeController, animated: false, completion: nil)
                }
            }
        } else {
            print("Registration Error : \(errormessage!)")
        }
    }
}
