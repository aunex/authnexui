//
//  DetailAccountCell.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/26/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class DetailAccountCell: UITableViewCell {
    
    let deviceWidth = UIScreen.main.bounds.width
    let deviceHeight = UIScreen.main.bounds.height
    let fontScale = UIScreen.main.bounds.width/375
    
    private var accountTitle: UILabel!
    private var accountNumber: UILabel!
    private var balanceTitle: UILabel!
    private var balance: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setcellUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension DetailAccountCell {
    fileprivate func setcellUI() {
        accountTitle = UILabel()
        accountTitle.translatesAutoresizingMaskIntoConstraints = false
        accountTitle.textAlignment = .left
        accountTitle.text = "Savings Account"
        accountTitle.font = UIFont(name: "Verdana", size: 20 * fontScale)
        addSubview(accountTitle)
        
        let leftconstant = deviceWidth/18.75
        
        let accounttitleConstraints = [accountTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: leftconstant),
                                       accountTitle.topAnchor.constraint(equalTo: topAnchor),
                                       accountTitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -leftconstant)]
        NSLayoutConstraint.activate(accounttitleConstraints)
        
        accountNumber = UILabel()
        accountNumber.translatesAutoresizingMaskIntoConstraints = false
        accountNumber.textAlignment = .left
        accountNumber.text = "85-5606-47-84"
        accountNumber.font = UIFont(name: "Verdana-Bold", size: 30 * fontScale)
        addSubview(accountNumber)
        
        let accountNumberConstraints = [accountNumber.leftAnchor.constraint(equalTo: leftAnchor, constant: leftconstant),
                                       accountNumber.topAnchor.constraint(equalTo: accountTitle.bottomAnchor),
                                       accountNumber.rightAnchor.constraint(equalTo: rightAnchor, constant: -leftconstant)]
        NSLayoutConstraint.activate(accountNumberConstraints)
        
        balanceTitle = UILabel()
        balanceTitle.translatesAutoresizingMaskIntoConstraints = false
        balanceTitle.textAlignment = .left
        balanceTitle.text = "Available Balance"
        balanceTitle.font = UIFont(name: "Verdana", size: 20 * fontScale)
        addSubview(balanceTitle)
        
        let balanceTitleConstraints = [balanceTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: leftconstant),
                                        balanceTitle.topAnchor.constraint(equalTo: accountNumber.bottomAnchor),
                                        balanceTitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -leftconstant)]
        NSLayoutConstraint.activate(balanceTitleConstraints)
        
        balance = UILabel()
        balance.translatesAutoresizingMaskIntoConstraints = false
        balance.textAlignment = .left
        balance.text = "$ 4978.80"
        balance.font = UIFont(name: "Verdana-Bold", size: 30 * fontScale)
        addSubview(balance)
        
        let balanceConstraints = [balance.leftAnchor.constraint(equalTo: leftAnchor, constant: leftconstant),
                                       balance.topAnchor.constraint(equalTo: balanceTitle.bottomAnchor),
                                       balance.rightAnchor.constraint(equalTo: rightAnchor, constant: -leftconstant)]
        NSLayoutConstraint.activate(balanceConstraints)
    }
}
