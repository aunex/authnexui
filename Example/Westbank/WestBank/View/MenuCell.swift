//
//  MenuCell.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class MenuCell: UICollectionViewCell {
    
    let deviceWidth = UIScreen.main.bounds.width
    let deviceHeigh = UIScreen.main.bounds.height
    let fontScale = UIScreen.main.bounds.width/375
    
    private var menutitle: UILabel!
    private var menuIcon: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setcellUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var rowindex: Int! {
        didSet {
            switch rowindex {
            case 0:
                menutitle.text = "Cardless ATM"
                contentView.backgroundColor = UIColor.white
                menutitle.textColor = .black
                menuIcon.image = UIImage(named: "atm")
            case 1:
                menutitle.text = "Mobile Banking"
                contentView.backgroundColor = UIColor.init(red: 0/255, green: 182/255, blue: 190/255, alpha: 1)
                menutitle.textColor = .white
                menuIcon.image = UIImage(named: "mb")
            case 2:
                menutitle.text = "Login With QR Code"
                contentView.backgroundColor = UIColor.init(red: 68/255, green: 8/255, blue: 99/255, alpha: 1)
                menutitle.textColor = .white
                menuIcon.image = UIImage(named: "qr")
            default:
                return
            }
        }
    }
}

extension MenuCell {
    fileprivate func setcellUI() {
        menuIcon = UIImageView()
        menuIcon.translatesAutoresizingMaskIntoConstraints = false
        menuIcon.contentMode = .scaleAspectFit
        menuIcon.backgroundColor = .clear
        addSubview(menuIcon)
        
        let menuiconRight = -deviceWidth/37.5
        let menuiconWidthHeight = deviceWidth/9.375
        let menuiconConstraints = [menuIcon.centerYAnchor.constraint(equalTo: centerYAnchor),
                                   menuIcon.rightAnchor.constraint(equalTo: rightAnchor, constant: menuiconRight),
                                   menuIcon.widthAnchor.constraint(equalToConstant: menuiconWidthHeight),
                                   menuIcon.heightAnchor.constraint(equalToConstant: menuiconWidthHeight)]
        
        NSLayoutConstraint.activate(menuiconConstraints)
        
        menutitle = UILabel()
        menutitle.translatesAutoresizingMaskIntoConstraints = false
        menutitle.textAlignment = .left
        menutitle.text = "Cardless ATM"
        menutitle.font = UIFont(name: "Verdana-Bold", size: 17 * fontScale)
        addSubview(menutitle)
        
        let menutitleleft = deviceWidth/17.05
        let menutitleconstraints = [menutitle.centerYAnchor.constraint(equalTo: centerYAnchor),
                                    menutitle.leftAnchor.constraint(equalTo: leftAnchor, constant: menutitleleft),
                                    menutitle.rightAnchor.constraint(equalTo: menuIcon.leftAnchor)]
        NSLayoutConstraint.activate(menutitleconstraints)
    }
}
