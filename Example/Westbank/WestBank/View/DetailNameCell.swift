//
//  DetailNameCell.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/26/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class DetailNameCell: UITableViewCell {
    
    let deviceWidth = UIScreen.main.bounds.width
    let deviceHeight = UIScreen.main.bounds.height
    let fontScale = UIScreen.main.bounds.width/375
    
    private var welcomeTitle: UILabel!
    private var welcomesubTitle: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setcellUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}

extension DetailNameCell {
    fileprivate func setcellUI() {
        welcomeTitle = UILabel()
        welcomeTitle.translatesAutoresizingMaskIntoConstraints = false
        welcomeTitle.text = "Welcom Anurdh"
        welcomeTitle.textAlignment = .center
        welcomeTitle.font = UIFont(name: "Verdana", size: 20 * fontScale)
        addSubview(welcomeTitle)
        
        let leftconstant = deviceWidth/18.75
        let topconstant = contentView.frame.height/5
        let welcometitleConstraints = [welcomeTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: leftconstant),
                                       welcomeTitle.topAnchor.constraint(equalTo: topAnchor, constant: topconstant)]
        NSLayoutConstraint.activate(welcometitleConstraints)
        
        welcomesubTitle = UILabel()
        welcomesubTitle.translatesAutoresizingMaskIntoConstraints = false
        welcomesubTitle.text = "Here's a summary of your accunts. You can \nscroll right to switch between accounts"
        welcomesubTitle.numberOfLines = 3
        welcomesubTitle.textAlignment = .left
        welcomesubTitle.font = UIFont(name: "Verdana", size: 15 * fontScale)
        addSubview(welcomesubTitle)
        
        let welcomesubTitleConstraints = [welcomesubTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: leftconstant),
                                          welcomesubTitle.topAnchor.constraint(equalTo: welcomeTitle.bottomAnchor, constant: topconstant)]
        NSLayoutConstraint.activate(welcomesubTitleConstraints)
    }
}
