//
//  SliderCell.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/25/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class SliderCell: UICollectionViewCell {
    
    let deviceWidth = UIScreen.main.bounds.width
    let deviceHeigh = UIScreen.main.bounds.height
    let fontScale = UIScreen.main.bounds.width/375
    
    private var mainimageView: UIImageView!
    private var headTitle: UILabel!
    private var subTitle: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setcellUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var rowIndex: Int! {
        didSet {
            switch rowIndex {
            case 0:
                mainimageView.image = UIImage(named: "one")
                headTitle.text = "You don't have to remember passwords any more"
                subTitle.text = "You can use biometrics instead of password login to WestBank online banking"
            case 1:
                mainimageView.image = UIImage(named: "two")
                headTitle.text = "Even you do not have to keep your user name in mind"
                subTitle.text = "If you are using a desktop computer, scan the QR code instead of your username.If you use the WestBank mobile banking application to login just authenticate with biometrics."
            case 2:
                mainimageView.image = UIImage(named: "three")
                headTitle.text = "Setting up takes few seconds"
                subTitle.text = "It doesn't bother you, just proceed further and provide your consent.We will not ask endless questions."
            default:
                return
            }
        }
    }
}

extension SliderCell {
    fileprivate func setcellUI() {
        contentView.backgroundColor = .white
        mainimageView = UIImageView()
        mainimageView.translatesAutoresizingMaskIntoConstraints = false
        mainimageView.backgroundColor = .clear
        mainimageView.contentMode = .scaleAspectFill
        addSubview(mainimageView)
        
        let imageviewHeight = deviceHeigh/2.66
        let mainimageviewConstraints = [mainimageView.leftAnchor.constraint(equalTo: leftAnchor),
                                        mainimageView.topAnchor.constraint(equalTo: topAnchor),
                                        mainimageView.rightAnchor.constraint(equalTo: rightAnchor),
                                        mainimageView.heightAnchor.constraint(equalToConstant: imageviewHeight)]
        NSLayoutConstraint.activate(mainimageviewConstraints)
        
        headTitle = UILabel()
        headTitle.translatesAutoresizingMaskIntoConstraints = false
        headTitle.numberOfLines = 3
        headTitle.textAlignment = .center
        headTitle.text = "Even you do not have to keep your user name in mind"
        headTitle.font = UIFont(name: "Verdana", size: 20 * fontScale)
        headTitle.textColor = .black
        addSubview(headTitle)
        
        let headtitleTop = deviceHeigh/22.23
        let headtitleleftright = deviceWidth/25
        let headtitleConstraints = [headTitle.centerXAnchor.constraint(equalTo: centerXAnchor),
                                   headTitle.topAnchor.constraint(equalTo: mainimageView.bottomAnchor, constant: 2 * headtitleTop),
                                   headTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: headtitleleftright),
                                   headTitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -headtitleleftright)]
        NSLayoutConstraint.activate(headtitleConstraints)
        
        subTitle = UILabel()
        subTitle.translatesAutoresizingMaskIntoConstraints = false
        subTitle.numberOfLines = 0
        subTitle.textAlignment = .center
        subTitle.text = "If you are using a desktop computer, scan the QR code instead of your username.If you use the WestBank mobile banking application to login just authenticate with biometrics."
        subTitle.font = UIFont(name: "Verdana", size: 15 * fontScale)
        subTitle.textColor = .black
        addSubview(subTitle)
        
        let subTitleTop = deviceHeigh/33.5
        let subTitleleftright = deviceWidth/25
        let subTitleConstraints = [subTitle.centerXAnchor.constraint(equalTo: centerXAnchor),
                                    subTitle.topAnchor.constraint(equalTo: headTitle.bottomAnchor, constant: subTitleTop),
                                    subTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: subTitleleftright),
                                    subTitle.rightAnchor.constraint(equalTo: rightAnchor, constant: -subTitleleftright)]
        NSLayoutConstraint.activate(subTitleConstraints)
    }
}

