//
//  Login.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/28/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit

class Login: NSObject {

    var UserName: String!
    var Passsword: String!
}

extension Login {
    func dictionary() -> [String: AnyObject] {
        var dictionary: [String: AnyObject] = [:]
        dictionary["Username"] = UserName as AnyObject
        dictionary["Password"] = Passsword as AnyObject
        return dictionary
    }
}

extension Login {
    func logintoWestBank(login: Login, completion:@escaping(Bool, String) ->()) {
        if login.UserName == "" || login.Passsword == "" {
            completion(false, "Username or Password is Empty")
            return
        }
        
        Network.sharedInstance.isConnectedToNetwork { (reachable) in
            if !reachable {
                completion(false, "Network connection appears to be offline")
                return
            }
            
            _ = login.dictionary()
            
            HttpClient.sharedInstance.loginWestBank(login: login, completion: { (success, err) in
                if success {
                    completion(true, "")
                    return
                } else {
                    completion(false, "Failed to login, Please try again")
                }
                
            })
        }
    }
}
