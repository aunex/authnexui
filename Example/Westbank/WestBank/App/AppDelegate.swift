//
//  AppDelegate.swift
//  WestBank
//
//  Created by Anuradh Caldera on 3/23/19.
//  Copyright © 2019 Anuradh Caldera. All rights reserved.
//

import UIKit
import AirshipKit
import AuthnexUI
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        IQKeyboardManager.shared.enable = true
        
        let config = UAConfig.default()
        UAirship.takeOff(config)
        UAirship.setLogging(false)
        UAirship.push().userPushNotificationsEnabled = true
        UAirship.push().backgroundPushNotificationsEnabled = true
        if #available(iOS 10.0, *) {
            UAirship.push().defaultPresentationOptions = [.alert, .badge, .sound]
        }
        UAirship.push()?.pushNotificationDelegate = self
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        let rootVC = RootController()
        window?.rootViewController = rootVC
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

//MARK: Implement push notification deletages
extension AppDelegate: UAPushNotificationDelegate {
    
    func receivedForegroundNotification(_ notificationContent: UANotificationContent, completionHandler: @escaping () -> Void) {
        
        if(AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
            AuthnexCore.sharedInctance.verification(content: notificationContent) { (success, approval , err, code) in
                if success {
                    print("user authenticated")
                    DispatchQueue.main.async {
                        let homeController = BanktoAuthnexAuthController()
                        homeController.authverification = true
                        homeController.authverificationErrmessage = nil
                        self.window?.rootViewController = homeController
                    }
                    return
                }
                if approval {
                    DispatchQueue.main.async {
                        // no approvals for WestBank App
                    }
                    return
                }
                print("user authentication fails : \(err!)")
                DispatchQueue.main.async {
                    let homeController = BanktoAuthnexAuthController()
                    homeController.authverification = true
                    homeController.authverificationErrmessage = err
                    homeController.authnexerrcode = code
                    self.window?.rootViewController = homeController
                }
                return
            }
        }
    }
    
    func receivedBackgroundNotification(_ notificationContent: UANotificationContent, completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if(AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
            AuthnexCore.sharedInctance.verification(content: notificationContent) { (success, approval , err, code) in
                if success {
                    print("user authenticated")
                    DispatchQueue.main.async {
                        let homeController = BanktoAuthnexAuthController()
                        homeController.authverification = true
                        homeController.authverificationErrmessage = nil
                        self.window?.rootViewController = homeController
                    }
                    return
                }
                if approval {
                    DispatchQueue.main.async {
                       // no approvals for WestBank App
                    }
                    return
                }
                print("user authentication fails : \(err!)")
                DispatchQueue.main.async {
                    let homeController = BanktoAuthnexAuthController()
                    homeController.authverification = true
                    homeController.authverificationErrmessage = err
                    homeController.authnexerrcode = code
                    self.window?.rootViewController = homeController
                }
                return
            }
        }
        completionHandler(.noData)
    }
    
    func receivedNotificationResponse(_ notificationResponse: UANotificationResponse, completionHandler: @escaping () -> Void) {
        
        if(AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
            AuthnexCore.sharedInctance.verification(content: notificationResponse.notificationContent) { (success, approval , err, code) in
                if success {
                    print("user authenticated")
                    DispatchQueue.main.async {
                        let homeController = BanktoAuthnexAuthController()
                        homeController.authverification = true
                        homeController.authverificationErrmessage = nil
                        self.window?.rootViewController = homeController
                    }
                    return
                }
                if approval {
                    // no approvals for WestBank App
                    return
                }
                print("user authentication fails : \(err!)")
                DispatchQueue.main.async {
                    let homeController = BanktoAuthnexAuthController()
                    homeController.authverification = true
                    homeController.authverificationErrmessage = err
                    homeController.authnexerrcode = code
                    self.window?.rootViewController = homeController
                }
                return
            }
        }
        completionHandler()
    }
}


