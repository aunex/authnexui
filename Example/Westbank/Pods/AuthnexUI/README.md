
# AuthnexUI SDK Guide

## **Setup**
Install the dependencies through [Cocoapods](https://cocoapods.org)
```ruby
pod 'AuthnexUI'
```
After the installation is done, please open `<project>.xcworkspace `


## **Usage**

### **User Registration with QR**
Use this method to register a user by scanning a QR code	
```swift
AuthnexCore.sharedInctance.AuthnexUserRegistration(userName, companyId, pushToken, mobileNumber, qrcodeString, completion: (Bool, String?) -> ())
```
`userName` : Any username.

`companyId` : ID given by Authnex after registering your company with Authnex.

`pushToken` : Channel ID given by UrbanAirship.

`mobileNumber` : Any mobile number.

`qrcodeString` : Scanned QR code string for registration. (Not the login QR).

`completion` : The completion block will get called with two arguments. `Bool` is to state whether registration is success or not. `String` will be the error message if there is any.


***Example***
```swift
let qrString = "/02/xiahc2Ecw3AvciOFMKA31hTCMzioSWtOrFExqlWjkMr2gRy9EuwF1egHr5BjPjTYMj5rXgu7o63cqDI2gg=="
AuthnexCore.sharedInctance.AuthnexUserRegistration(userName: "User_Name_Any", companyId: <your_company_id>, pushToken: "<UAchannelID>", mobileNumber: "07xxxxxxx", qrcodeString: qrString) { (success, errorMessage) in
    if success {
        // Do something here
    } else {
        if let error = errorMessage {
            // Handle the error
            print(error)
        }
    }
}
```

***

### **User Registration (normal)**
Use this to register a user without having to scan a QR code.
```swift
AuthnexCore.sharedInctance.AuthnexUserNormalRegistration(userName, mappeduserId, companyId, pushToken, mobileNumber, completion: (Bool, String?) -> ())
```
`userName` : Any username.

`mappeduserId` : Unique ID for the registering user (generally an email address).

`companyId` : ID given by Authnex after registering your company with Authnex.

`pushToken` : Channel ID returned by UrbanAirship.

`mobileNumber` : Any mobile number.

`completion` : The completion block will get called with two arguments. `Bool` is to state whether registration is success or not. `String` will be the error message if there is any.


***Example***
```swift
AuthnexCore.sharedInctance.AuthnexUserNormalRegistration(userName: "User_Name_Any", mappeduserId: "user_company_email", companyId: <your_company_id>, pushToken: "<UAchannelID>", mobileNumber: "<user_company_mobile_number>") { (success, errorMessage) in
    if success {
        // Do something here
    } else {
        if let error = errorMessage {
            // Handle the error
            print(error)
        }
    }
}
```
***
### **Authentication**
Use this method to authenticate login.
```swift
AuthnexCore.sharedInctance.AuthnexqrLogin(qrcodeString, completion: (Bool, String?, Int?) -> ())
```
`qrcodeString` : Scanned login QR code.

`completion` : The completion block will get called with three arguments. `Bool` is to state whether login is success or not. `String` will be the error message if there is any. `Int` will be the error code.

***Example***
```swift
let loginQR = "270EOo4V8dV+WJ5VjglPYW0uSlJn8Y06Ls4AY1xGTHDP3hp8g24+lhHS0G4X7QVV"
AuthnexCore.sharedInctance.AuthnexqrLogin(qrcodeString: loginQR) { (success, errorMessage, code) in
    if success {
        // User is authenticated. Do something here.
    } else {
        if let error = errorMessage {
            // Handle the error
            print("Error in user login with qr :", error)
        }
    }
}
```
